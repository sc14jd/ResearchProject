
/*
 * Created on 16th January 2018
 * By Jordan Day
 * Class to represent a single Cell within the automaton
 */
public class Cell {

    int cellType;
    int cellHealth = 0;


    public int getCellType() {
        return cellType;
    }

    public void setCellType(int cellType) {
        this.cellType = cellType;
    }

    public int getCellHealth() { return cellHealth; }

    public void setCellHealth(int cellHealth) { this.cellHealth = cellHealth; }


    public Cell() {}


    double bindingConc = 0.00260000000;             // C0
    double interstitialConc;                        // C1
    double intracellularFree;                       // C2
    double intracellularBound;                      // C3
    double absorptionValue = 0.0;
    boolean drugActive = false;
    boolean aggressive = true;

    public double getBindingConc() {
        return bindingConc;
    }

    public double getInterstitialConc() {
        return interstitialConc;
    }

    public void setInterstitialConc(double interstitialConc) {
        this.interstitialConc = interstitialConc;
    }

    public double getIntracellularFree() {
        return intracellularFree;
    }

    public void setIntracellularFree(double intracellularFree) {
        this.intracellularFree = intracellularFree;
    }

    public double getIntracellularBound() {
        return intracellularBound;
    }

    public void setIntracellularBound(double intracellularBound) {
        this.intracellularBound = intracellularBound;
    }

    public double getAbsorptionValue() { return absorptionValue; }

    public void setAbsorptionValue(double absorptionValue) { this.absorptionValue = absorptionValue; }

    public boolean isDrugActive() { return drugActive; }

    public void setDrugActive(boolean drugActive) { this.drugActive = drugActive; }

    public boolean isAggressive() { return aggressive; }

    public void setAggressive(boolean aggressive) {
        this.aggressive = aggressive;
    }
}
