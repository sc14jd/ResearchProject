import javax.swing.*;

/*
 * Created on 16th January 2018
 * By Jordan Day
 * Class to represent a single CellButton and it's position in
 * the JButton grid
 */
public class CellButton extends JButton {

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) { this.x = x; }

    public void setY(int y) {
        this.y = y;
    }
}
