import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.math.BigDecimal;

/*
 * Created on 16th January 2018
 * By Jordan Day
 * User-select screen for deciding which type of cells exist where on the tissue
 */
public class CellGUI extends JFrame {


    Cell[][] cells;  // Cells selected on button screen
    Cell[][] cellsExpanded; // All represented cells on the grid
    JButton startButton;

    JPanel[][] startGrid;

    int gridSize;
    int stepTime;
    int generations;
    String drugMode = "";
    String cancerMode = "";
    double cellPermeability;            // k0
    double membranePermeability;        // k1
    double associationRate;             // k2
    double dissociationRate;            // k-2
    double timeStep;                    // dt
    double bindingThreshold;

    // Setter methods for variables being passed from CellularAutomata / DiffusionScreen
    public void setGridSize(int gridSize) { this.gridSize = gridSize; }

    public void setStepTime(int stepTime) {
        this.stepTime = stepTime;
    }

    public void setDrugMode(String drugMode) {
        this.drugMode = drugMode;
    }

    public void setCancerMode(String cancerMode) {
        this.cancerMode = cancerMode;
    }

    public void setGenerations(int generations) {
        this.generations = generations;
    }

    public void setCellPermeability(double cellPermeability) {
        this.cellPermeability = cellPermeability;
    }

    public void setMembranePermeability(double membranePermeability) {
        this.membranePermeability = membranePermeability;
    }

    public void setAssociationRate(double associationRate) {
        this.associationRate = associationRate;
    }

    public void setDissociationRate(double dissociationRate) {
        this.dissociationRate = dissociationRate;
    }

    public void setTimeStep(double timeStep) {
        this.timeStep = timeStep;
    }

    public void setBindingThreshold(double bindingThreshold) {
        this.bindingThreshold = bindingThreshold;
    }

    public CellGUI() { }

    public void CreateGrid() {

        int guiSize = gridSize/10;

        //System.out.println(guiSize);
        JPanel mainGrid = new JPanel(new GridLayout(guiSize, guiSize));
        startGrid = new JPanel[guiSize][guiSize];

        // Initialise 2D grid of Cells
        cells = new Cell[guiSize][guiSize];
        for (int row = 0; row < guiSize; row++) {
            for (int col = 0; col < guiSize; col++) {
                cells[row][col] = new Cell();
            }
        }

        cellsExpanded = new Cell[gridSize][gridSize];
        for (int row = 0; row < gridSize; row++) {
            for (int col = 0; col < gridSize; col++) {
                cellsExpanded[row][col] = new Cell();
                cellsExpanded[row][col].setCellType(0);
            }
        }

        CellButton[][] buttonArray = new CellButton[guiSize][guiSize];

        /* cellType 0 = regular
         * cellType 1 = cancerous
         * cellType 2 = empty cell
         */

        // The ActionListener for the entire 2D array of JButtoms
        // Allows us to locate which button was pressed and what to change about it
        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CellButton thisButton = (CellButton) e.getSource();
                int xCoord = thisButton.getX();
                int yCoord = thisButton.getY();
                int thisCellType = cells[xCoord][yCoord].getCellType();
                    switch (thisCellType) {
                        case 0:
                            cells[xCoord][yCoord].setCellType(1);
                            buttonArray[xCoord][yCoord].setBackground(Color.ORANGE);
                            buttonArray[xCoord][yCoord].setText("Cancerous");
                            for (int i = 0; i < 10; ++i) {
                                for (int j = 0; j < 10; ++j) {
                                    cellsExpanded[i+(xCoord*10)][j+(yCoord*10)] = cells[xCoord][yCoord];
                                }
                            }
                            break;
                        case 1:
                            cells[xCoord][yCoord].setCellType(2);
                            buttonArray[xCoord][yCoord].setBackground(Color.BLACK);
                            buttonArray[xCoord][yCoord].setText("Empty");
                            for (int i = 0; i < 10; ++i) {
                                for (int j = 0; j < 10; ++j) {
                                    cellsExpanded[i+(xCoord*10)][j+(yCoord*10)] = cells[xCoord][yCoord];
                                }
                            }
                            break;
                        case 2:
                            cells[xCoord][yCoord].setCellType(0);
                            buttonArray[xCoord][yCoord].setBackground(Color.BLUE);
                            buttonArray[xCoord][yCoord].setText("Healthy");
                            for (int i = 0; i < 10; ++i) {
                                for (int j = 0; j < 10; ++j) {
                                    cellsExpanded[i+(xCoord*10)][j+(yCoord*10)] = cells[xCoord][yCoord];
                                }
                            }
                            break;

                        default:
                            // Error
                            System.out.println("Unexpected cell type!");
                            break;
                }
            }
        };

        startButton = new JButton("Begin Automaton");
        startButton.setToolTipText("Begins the automaton with specified parameters");

        // Initialise each JPanel as a Healthy Cell [type 0]
        for (int j = 0; j < guiSize; j++) {
            for (int i = 0; i < guiSize; i++) {
                startGrid[i][j] = new JPanel(new GridLayout(1, 1, 1, 1));
                startGrid[i][j].setBackground(Color.BLUE);
                startGrid[i][j].setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
                mainGrid.add(startGrid[i][j]);
                cells[i][j].setCellType(0);
            }
        }

        // GridBagLayout acts as a grid to store components in particular cells
        GridBagLayout masterLayout = new GridBagLayout();
        this.setLayout(masterLayout);


        // GridBagConstraints allows us to manipulate each button within the grid, allowing us
        // to change things such as the amount of cells it occupies
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.weightx = 0.5;
        constraints.weighty = 0.5;
        constraints.fill = GridBagConstraints.BOTH;

        // Initialise each JButton as a Healthy Cell [type 0]
        for (int row = 0; row < guiSize; row++) {
            for (int col = 0; col < guiSize; col++) {
                buttonArray[row][col] = new CellButton();
                buttonArray[row][col].setX(row);
                buttonArray[row][col].setY(col);
                buttonArray[row][col].setBackground(Color.BLUE);
                buttonArray[row][col].setText("Healthy");
                buttonArray[row][col].setOpaque(true);
                buttonArray[row][col].addActionListener(listener);

                startGrid[row][col].add(buttonArray[row][col], constraints);
            }
        }



        // Insert the JButton grid at the top
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.anchor = GridBagConstraints.CENTER;
        this.add(mainGrid, constraints);

        // Insert the Start Button below it
        constraints.weighty = 0.1;
        constraints.gridx = 0;
        constraints.gridy = 1;
        this.add(startButton, constraints);


        // The ActionListener for the start button.
        // Allows us to create the final Cell grid based on the grid created by the user
        startButton.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed (ActionEvent e) {
                // cellsExpanded logic expands the smaller cells 2D array to be the
                // size the user opted for on the CellularAutomata screen.
                // NOTE: This is done as a 100x100 grid of JButtons would look very
                // distorted, and respond much slower

                dispose();

                // From here, we can only go to the CellSimulation window
                // We therefore transfer all necessary variables through here
                CellSimulation simulation = new CellSimulation();
                simulation.setGridSize(gridSize);
                simulation.setStepTime(stepTime);
                simulation.setGenerations(generations);
                if (drugMode == "Uniform") {
                    simulation.setUniform(true);
                } else {
                    simulation.setUniform(false);
                    simulation.setCellPermeability(cellPermeability);
                    simulation.setMembranePermeability(membranePermeability);
                    simulation.setAssociationRate(associationRate);
                    simulation.setDissociationRate(dissociationRate);
                    simulation.setTimeStep(timeStep);
                    simulation.setBindingThreshold(bindingThreshold);

                }

                if (cancerMode == "Probability") {
                    simulation.setRandom(true);
                } else {
                    simulation.setRandom(false);
                }

                // Set the expanded cells to the simulation's 2D array of cells
                simulation.setAllCells(cellsExpanded);
                simulation.CreateWindow();
                simulation.setVisible(true);
                simulation.StepThroughGenerations();
            }
        });
    }

    // Create the window that has been set up previously
    public void CreateWindow() {
        CreateGrid();

        // Window features such as size and how to handle exiting the window
        this.setMinimumSize(new Dimension(720, 600));
        this.setSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width-20, Toolkit.getDefaultToolkit().getScreenSize().height-80));
        setTitle("Cell Selection Screen");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
