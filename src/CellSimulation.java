import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * Created on 15th January 2018
 * By Jordan Day
 * Cellular Automaton simulation logic and visualisation
 */
public class CellSimulation extends JFrame {

    // Declaring arrays to represent:
    Cell[][] neighbourMatrix;        // All of the cells extended to
                                     // generalise border cells
    JPanel[][] startGrid;            // The visual representation of
                                     // all cells as a JPanel grid

    JLabel[][] labels;

    int[][][] counts;                // A 3D array to represent the number
                                     // of each neighbour type at each cell
    int[][] neighbourAggressions;    // A 2D array to give each cell's amount
                                     // of aggressive cancerous neighbours, and
                                     // therefore calculate passive cancerous
                                     // neighbours

    int currentGeneration = 0;       // Initialise the initial generation



    int generations;                 // Final total of generations to step over
    // Arrays to store values for GenerateReport class graphs
    double[] cancerousCounts;        // Count of cancerous cells
    double[] absorbedCounts;         // Count of cells that have 'absorbed' drug
    double[] totalAbsorptions;       // Sums of total absorbed concentration

    public void setGenerations(int generations) {
        this.generations = generations;
        this.cancerousCounts = new double[generations];
        this.absorbedCounts = new double[generations];
        this.totalAbsorptions = new double[generations];
    }

    JLabel genLabel;
    JRadioButton healthButton;
    JRadioButton diffusionButton;
    JButton startButton;
    JButton analysisButton;
    boolean healthText = true;

    // Initialising variables which determine a given cell's next type
    int cancerCount = 0;
    int cellHealth = 0;         // Current cells health to be manipulated
                                // at each generation
    String healthString = "";
    Timer timer;

    double minimumBound = 100.0;

    // Drug diffusion variables as doubles
    double cellPermeability;            // k0
    double membranePermeability;        // k1
    double associationRate;             // k2
    double dissociationRate;            // k-2
    double timeStep;                    // dt
    double bindingThreshold;


    public void setCellPermeability(double cellPermeability) {
        this.cellPermeability = cellPermeability;
    }

    public void setMembranePermeability(double membranePermeability) { this.membranePermeability = membranePermeability; }

    public void setAssociationRate(double associationRate) {
        this.associationRate = associationRate;
    }

    public void setDissociationRate(double dissociationRate) {
        this.dissociationRate = dissociationRate;
    }

    public void setTimeStep(double timeStep) {
        this.timeStep = timeStep;
    }

    public void setBindingThreshold(double bindingThreshold) { this.bindingThreshold = bindingThreshold; }

    // The areas and volumes are fixed for this model
    double interfaceArea = 0.000000000002000000;  //Aij       // Moderately arbitrary
    double internalArea = 0.000000000006000000;   //a(i)      // Moderately arbitrary
    double interstitialVolume = 0.00000000000000000066; //d1Vi
    double intracellularVolume = 0.000000000000000002;    //d2Vi

    // Arrays to store each cell's concentration changes at each diffusion step
    double[][] c1changes;
    double[][] c2changes;
    double[][] c3changes;

    // Charting variables
    int absorbedCount = 0;
    double totalAbsorption = 0.0;

    // Some variables that are always passed from first screen
    Cell currentCell = new Cell();
    int gridSize;
    boolean uniform;
    boolean random;
    int stepTime = 3;
    Cell[][] allCells;

    public void setGridSize(int gridSize) { this.gridSize = gridSize; }

    public void setAllCells(Cell[][] allCells) { this.allCells = allCells; }

    public void setUniform(boolean uniform) { this.uniform = uniform; }

    public void setRandom(boolean random) { this.random = random; }

    public void setStepTime(int stepTime) {
        this.stepTime = stepTime;
    }


    public CellSimulation() {

        // Set default values for diffusion model, if the user chose none

        if (cellPermeability == 0.0) {
            cellPermeability = 0.0000025;           // k0
        }
        if (membranePermeability == 0.0) {
            membranePermeability = 0.000001;        // k1
        }
        if (associationRate == 0.0) {
            associationRate = 0.000000000009;       // k2
        }
        if (dissociationRate == 0.0) {
            dissociationRate = 0.00014;             // k-2
        }
        if (timeStep == 0.0) {
            timeStep = 1;                         // dt
        }
        if (bindingThreshold == 0.0) {
            bindingThreshold = Math.pow(10, -30);
        }

    }

    // Initialises and allocates window components to correct place on screen
    public void CreateGrid() {
        int rows = allCells.length;
        int cols = allCells[0].length;

        GridLayout cellGrid = new GridLayout(rows, cols);
        this.setLayout(cellGrid);

        JPanel mainGrid = new JPanel(cellGrid);
        startGrid = new JPanel[rows][cols];

        // Cell types:
        /* cellType 0 = healthy
         * cellType 1 = cancerous
         * cellType 2 = empty cell
         */

        // Place JPanel for each cell in grid, give a suitable border also
        for (int j = 0; j < cols; j++) {
            for (int i = 0; i < rows; i++) {
                startGrid[i][j] = new JPanel(new GridLayout(1, 1, 1, 1));
                startGrid[i][j].setBorder(BorderFactory.createLineBorder(Color.WHITE));
                mainGrid.add(startGrid[i][j]);
            }
        }

        this.add(mainGrid, BorderLayout.CENTER);

        // GridBagLayout allows us to insert the grid from above,
        // and then add buttons and labels below
        GridBagLayout masterLayout = new GridBagLayout();
        this.setLayout(masterLayout);

        // GridBagConstraints allows us to manipulate each component within the grid, allowing us
        // to change things such as the amount of cells it occupies
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(1, 1, 1, 1);
        constraints.fill = GridBagConstraints.BOTH;

        // This block adds the actual automaton to the screen
        constraints.weightx = 0.5;
        constraints.weighty = 0.5;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        this.add(mainGrid, constraints);


        constraints.weighty = 0;
        constraints.weightx = 0.5;
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        genLabel = new JLabel("Current generation: " + (currentGeneration+1) + " / " + generations, SwingConstants.CENTER);
        genLabel.setAlignmentY(Component.CENTER_ALIGNMENT);
        this.add(genLabel, constraints);


        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        healthButton = new JRadioButton("Display cell healths");
        healthButton.setHorizontalAlignment(AbstractButton.CENTER);
        this.add(healthButton, constraints);

        constraints.gridx = 1;
        constraints.gridy = 2;
        diffusionButton = new JRadioButton("Display percentages of total drug absorbed");
        diffusionButton.setHorizontalAlignment(AbstractButton.CENTER);
        this.add(diffusionButton, constraints);

        ButtonGroup cellTexts = new ButtonGroup();
        cellTexts.add(healthButton);
        cellTexts.add(diffusionButton);
        healthButton.setSelected(true);


        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 1;
        startButton = new JButton("Begin simulation");
        startButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(startButton, constraints);

        constraints.gridx = 1;
        constraints.gridy = 3;
        analysisButton = new JButton("Open analysis tool [after simulation]");
        analysisButton.setEnabled(false);
        analysisButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.add(analysisButton, constraints);

        labels = new JLabel[allCells.length][allCells[0].length];
        AddLabels();

        // Update initial grid visually, and certain variables such as health
        // based on the cell's type
        for (int col = 0; col < cols; col++) {
            for (int row = 0; row < rows; row++) {
                switch (allCells[row][col].getCellType()) {
                    case 0:
                        startGrid[row][col].setBackground(Color.BLUE);
                        allCells[row][col].setCellHealth(100);
                        break;
                    case 1:
                        startGrid[row][col].setBackground(Color.ORANGE);
                        cancerCount++;
                        break;
                    case 2:
                        startGrid[row][col].setBackground(Color.BLACK);
                        allCells[row][col].setCellHealth(100);
                        break;
                    default:
                        // Error here
                        System.out.println("ERROR: Cell type = :" +allCells[row][col].getCellType());
                        startGrid[row][col].setBackground(Color.WHITE);
                        break;
                }
            }
        }
        cancerousCounts[0] = cancerCount;

        // Initialise neighbourMatrix
        neighbourMatrix = new Cell[rows + 2][cols + 2];
        for (int i = 0; i < neighbourMatrix.length; i++) {
            for (int j = 0; j < neighbourMatrix[i].length; j++) {
                neighbourMatrix[i][j] = new Cell();
            }
        }

        // ActionListeners to switch between text displayed in each cell
        healthButton.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent e) {
                healthText = true;
            }
        });

        diffusionButton.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent e) {
                healthText = false;
            }
        });

        // Begins the timer, which starts the simulation
        startButton.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent e) {
                timer.start();
                startButton.setEnabled(false);
            }
        });

        // Allows us to generate a report suited to the user's
        // choice of simulation, when they are ready
        analysisButton.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent e) {
                GenerateReport report = new GenerateReport();
                report.setGridSize(gridSize);
                report.setUniform(uniform);
                report.setRandom(random);
                report.setCancerCounts(cancerousCounts);
                report.setAbsorbedCounts(absorbedCounts);
                report.setTotalAbsorptions(totalAbsorptions);
                if (!uniform) {
                    report.setCellPermeability(cellPermeability);
                    report.setMembranePermeability(membranePermeability);
                    report.setAssociationRate(associationRate);
                    report.setDissociationRate(dissociationRate);
                    report.setTimeStep(timeStep);
                    report.setBindingThreshold(bindingThreshold);
                }
                dispose();
                report.CreateReportWindow();
            }
        });
    }

    // Gets the amount of each type of neighbour at each iteration,
    // so we can see how the cell's next state will be affected
    public void GetNeighbourCounts() {

        Cell[] neighbour;
        int count_0;
        int count_1;
        int count_2;
        int aggression;

        counts = new int[3][allCells.length][allCells[0].length];
        neighbourAggressions = new int[allCells.length][allCells[0].length];

        // Update neighbourMatrix
        for (int i = 0; i < allCells.length; i++) {
            for (int j = 0; j < allCells[i].length; j++) {
                neighbourMatrix[i + 1][j + 1] = allCells[i][j];
            }
        }

        for (int i = 0; i < allCells.length; i++) {
            for (int j = 0; j < allCells[i].length; j++) {
                neighbour = new Cell[8];
                count_0 = 0;
                count_1 = 0;
                count_2 = 0;
                aggression = 0;

                // Array of current cell's 8 neighbours
                neighbour[0] = neighbourMatrix[i][j];
                neighbour[1] = neighbourMatrix[i + 1][j];
                neighbour[2] = neighbourMatrix[i + 2][j];
                neighbour[3] = neighbourMatrix[i][j + 1];
                neighbour[4] = neighbourMatrix[i + 2][j + 1];
                neighbour[5] = neighbourMatrix[i][j + 2];
                neighbour[6] = neighbourMatrix[i + 1][j + 2];
                neighbour[7] = neighbourMatrix[i + 2][j + 2];

                for (Cell cell : neighbour) {
                    switch (cell.getCellType()) {
                        case 0:
                            count_0 += 1;
                            break;
                        case 1:
                            count_1 += 1;
                            if (cell.isAggressive()) {
                                aggression += 1;
                            }
                            break;
                        case 2:
                            count_2 += 1;
                            break;

                        default:
                            break;

                    }
                }

                // Index at counts[x] is the count of that
                // cell type for a given i,j coordinate
                counts[0][i][j] = count_0;
                counts[1][i][j] = count_1;
                counts[2][i][j] = count_2;
                neighbourAggressions[i][j] = aggression;
            }
        }
    }


    // Creates the grid specified above, also adding specific values for
    // screen size etc.
    public void CreateWindow() {
        CreateGrid();

        this.setMinimumSize(new Dimension(720, 600));
        this.setSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width-20, Toolkit.getDefaultToolkit().getScreenSize().height-80));
        setTitle("Cellular Automaton Simulation");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    // The simulation's main function, that uses a Timer to iterate over the generations
    public void StepThroughGenerations() {
        GetNeighbourCounts();
        // Update the initial drug concentrations accordingly
        if (uniform == true) {
            IntroduceUniformDrug();
        } else {
            IntroduceBoundaryDrug();
        }
        // Updates all cells before beginning the simulation
        try {
            Thread.sleep(1000);
        } catch (Exception ex) {
        }
        ActionListener timerListener = null;
        timerListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // We only need to do the diffusion calculations for the diffusion model
                if (uniform == false) {
                    DetermineDiffusedStates();
                }

                // 2 layers applied to the automaton model
                UpdateCells();
                SpreadCancer();

                // Update variables the user will see, and arrays for GenerateReport
                currentGeneration++;
                genLabel.setText("Current Generation: " + (currentGeneration+1) + " / " + generations);
                cancerousCounts[currentGeneration] = cancerCount;
                absorbedCounts[currentGeneration] = absorbedCount;
                // Rounding is done here so the axis labels are more concise
                String totalAbsorptionString = String.format("%3.2e",totalAbsorption);
                double totalAbsorptionRounded = Double.valueOf(totalAbsorptionString);
                totalAbsorptions[currentGeneration] = totalAbsorptionRounded;

                // Stops once we reach the amount of generations the user requested
                if (currentGeneration == generations-1) {
                    ((Timer) e.getSource()).stop();
                    analysisButton.setEnabled(true);
                }
            }
        };

        // stepTime*1000 is the delay between iterations in milliseconds
        timer = new Timer(stepTime*1000, timerListener);
        timer.setRepeats(true);
    }

    // Adds JLabels to each cell in the grid, to give the user real-time cell information
    public void AddLabels() {
        for (int i = 0; i < allCells.length; i++) {
            for (int j = 0; j < allCells[i].length; j++) {
                labels[i][j] = new JLabel();
                // Adjust font size based on grid size
                switch (gridSize) {
                    case (10):
                        labels[i][j].setFont(new Font("Arial", 0, 12));
                        break;
                    case (20):
                        labels[i][j].setFont(new Font("Arial", 0, 12));
                        break;
                    case (30):
                        labels[i][j].setFont(new Font("Arial", 0, 10));
                        break;
                    case (40):
                        labels[i][j].setFont(new Font("Arial", 0, 9));
                        break;
                    case (50):
                        labels[i][j].setFont(new Font("Arial", 0, 8));
                        break;
                    case (60):
                        labels[i][j].setFont(new Font("Arial", 0, 6));
                        break;
                    case (70):
                        labels[i][j].setFont(new Font("Arial", 0, 5));
                        break;
                    case (80):
                        labels[i][j].setFont(new Font("Arial", 0, 4));
                        break;
                    case (90):
                        labels[i][j].setFont(new Font("Arial", 0, 4));
                        break;
                    case (100):
                        labels[i][j].setFont(new Font("Arial", 0, 3));
                        break;
                }

                labels[i][j].setHorizontalAlignment(SwingConstants.CENTER);
                labels[i][j].setVerticalAlignment(SwingConstants.CENTER);

                if (currentCell.getCellType() != 1) {
                    labels[i][j].setForeground(Color.WHITE);
                }
                startGrid[i][j].add(labels[i][j]);
            }
        }
    }


    // 2 Drug introduction functions:

    // Introduce the drug to all cells and bind it to all cells
    // it can be bound to
    public void IntroduceUniformDrug() {
        for (Cell[] cellRow : allCells) {
            for (Cell cell : cellRow) {
                cell.setInterstitialConc(cell.getBindingConc());
                cell.setIntracellularFree(cell.getBindingConc());
                if (cell.getCellType() != 2) {
                    cell.setIntracellularBound(cell.getBindingConc());
                }
            }
        }
    }

    // Introduce the drug and bind it to all boundary cells of the tissue
    public void IntroduceBoundaryDrug() {
        for (Cell cell : neighbourMatrix[0]) {
            cell.setInterstitialConc(cell.getBindingConc());
            cell.setIntracellularFree(cell.getBindingConc());
            cell.setIntracellularBound(cell.getBindingConc());

        }

        for (int i = 1; i < neighbourMatrix.length-1; i++) {
            neighbourMatrix[i][0].setInterstitialConc(neighbourMatrix[i][0].getBindingConc());//.divide(BigDecimal.valueOf(2), 12, BigDecimal.ROUND_HALF_UP));
            neighbourMatrix[i][neighbourMatrix.length-1].setInterstitialConc(neighbourMatrix[i][neighbourMatrix.length-1].getBindingConc());//.divide(BigDecimal.valueOf(2), 12, BigDecimal.ROUND_HALF_UP));

            neighbourMatrix[i][0].setIntracellularFree(neighbourMatrix[i][0].getBindingConc());//.divide(BigDecimal.valueOf(3), 12, BigDecimal.ROUND_HALF_UP));
            neighbourMatrix[i][neighbourMatrix.length-1].setIntracellularFree(neighbourMatrix[i][neighbourMatrix.length-1].getBindingConc());//.divide(BigDecimal.valueOf(3), 12, BigDecimal.ROUND_HALF_UP));

            neighbourMatrix[i][0].setIntracellularBound(neighbourMatrix[i][0].getBindingConc());
            neighbourMatrix[i][neighbourMatrix.length-1].setIntracellularBound(neighbourMatrix[i][neighbourMatrix.length-1].getBindingConc());
        }

        for (Cell cell : neighbourMatrix[neighbourMatrix.length-1]) {
            cell.setInterstitialConc(cell.getBindingConc());
            cell.setIntracellularFree(cell.getBindingConc());
            cell.setIntracellularBound(cell.getBindingConc());
        }
    }


    // Jacobi method for calculating concentrations for all cells
    public void DetermineDiffusedStates() {
        GetNeighbourCounts();

        int i = 0;
        int j = 0;

        double c1change;
        double c2change;
        double c3change;

        double currentNeighbourCalc;
        double totalNeighbourCalc = 0.0;
        double vesselCalc = 0.0;
        double internalCalcC1;
        double internalCalcC2;

        double subtractable;

        c1changes = new double[allCells.length][allCells[0].length];
        c2changes = new double[allCells.length][allCells[0].length];
        c3changes = new double[allCells.length][allCells[0].length];

        double pkProfile = 0.0000000385802;

        for (Cell[] cellRow : allCells) {
            for (Cell cell : cellRow) {
                // Rate of change of C1 - Interstitial concentration
                Cell[] neighbourCells = new Cell[8];
                neighbourCells[0] = neighbourMatrix[i][j];
                neighbourCells[1] = neighbourMatrix[i + 1][j];
                neighbourCells[2] = neighbourMatrix[i + 2][j];
                neighbourCells[3] = neighbourMatrix[i][j + 1];
                neighbourCells[4] = neighbourMatrix[i + 2][j + 1];
                neighbourCells[5] = neighbourMatrix[i][j + 2];
                neighbourCells[6] = neighbourMatrix[i + 1][j + 2];
                neighbourCells[7] = neighbourMatrix[i + 2][j + 2];

                for (Cell neighbourCell : neighbourCells) {
                    currentNeighbourCalc = interfaceArea * cellPermeability * (neighbourCell.getInterstitialConc() - cell.getInterstitialConc());
                    totalNeighbourCalc += currentNeighbourCalc;
                }

                // Note: for my model, we can omit vesselCalc. I have left the logic here if someone wished to include it.
                // Zero flux assumed in Groh's paper, and we can assume there is
                // no vessel located by the cells of this tissue

                /*
                vesselCalc = pkProfile - cell.getInterstitialConc();
                vesselCalc *= vesselPermeability;
                vesselCalc *= interfaceArea;
                */

                internalCalcC1 = internalArea * membranePermeability * (cell.getIntracellularFree() - cell.getInterstitialConc());

                c1change = totalNeighbourCalc + vesselCalc + internalCalcC1;
                c1change /= interstitialVolume;

                // Setting boundaries for C1
                double interstitialConc = cell.getInterstitialConc();
                interstitialConc = interstitialConc + (c1change * timeStep);
                if (interstitialConc < 0) {
                    interstitialConc = 0;
                }

                if (interstitialConc > cell.getBindingConc()) {
                    interstitialConc = cell.getBindingConc();
                }

                c1changes[i][j] = interstitialConc;


                // Rate of change of C2 - Intracellular free concentration
                internalCalcC2 = internalArea * membranePermeability * (cell.getInterstitialConc() - cell.getIntracellularFree());
                c2change = internalCalcC2;

                subtractable = associationRate * cell.getIntracellularFree() * (cell.getBindingConc() - cell.getIntracellularBound());

                double dissociation = dissociationRate * cell.getIntracellularBound();
                subtractable -= dissociation;
                subtractable *= intracellularVolume;

                c2change -= subtractable;
                c2change /= intracellularVolume;

                // Setting boundaries for C2
                double intracellularFree = cell.getIntracellularFree();
                intracellularFree = intracellularFree + (c2change * timeStep);
                if (intracellularFree < 0) {
                    intracellularFree = 0;
                }
                //
                if (intracellularFree > cell.getBindingConc()) {
                    intracellularFree = cell.getBindingConc();
                }

                c2changes[i][j] = intracellularFree;

                // If cell isn't empty i.e. can absorb the drug
                if (cell.getCellType() != 2) {
                    // Rate of change of C3 - Intracellular bound concentration
                    c3change = subtractable;
                    c3change /= intracellularVolume;

                    // Setting boundaries for C3
                    double intracellularBound = cell.getIntracellularBound();
                    intracellularBound = intracellularBound + (c3change * timeStep);
                    if (intracellularBound < 0) {
                        intracellularBound = 0;
                    }
                    if (intracellularBound > cell.getBindingConc()) {
                        intracellularBound = cell.getBindingConc();
                    }
                    c3changes[i][j] = intracellularBound;

                    if (intracellularBound < minimumBound) {
                        minimumBound = intracellularBound;
                    }
                }
                totalNeighbourCalc = 0.0;
                j += 1;
            }
            i += 1;
            j = 0;
        }
    }

    // Updates the cells according to their type, as well as report arrays
    public void UpdateCells() {
        GetNeighbourCounts();
        int i = 0;
        int j = 0;


        double absorptionPercentage;

        absorbedCount = 0;
        totalAbsorption = 0.0;
        cancerCount = 0;

        for (Cell[] cellRow : allCells) {
            for (Cell cell : cellRow) {

                // For each cell that can absorb the drug
                if (cell.getCellType() != 2) {
                    if (uniform == false) {
                        cell.setInterstitialConc(c1changes[i][j]);
                        cell.setIntracellularFree(c2changes[i][j]);
                        cell.setIntracellularBound(c3changes[i][j]);
                    }

                    absorptionPercentage = cell.getIntracellularBound() / cell.getBindingConc();
                    absorptionPercentage *= 100;
                    cell.setAbsorptionValue(absorptionPercentage);



                    if ((cell.getIntracellularBound() > 0.0)) {
                        totalAbsorption += cell.getIntracellularBound();
                    }

                    if (cell.getIntracellularBound() > bindingThreshold) {
                        cell.setDrugActive(true);
                        absorbedCount += 1;
                    }
                }

                // For each non-cancerous cell
                if (cell.getCellType() != 1) {

                    cellHealth = cell.getCellHealth();
                    // How the drug being absorbed affects a healthy cell
                    if (cell.isDrugActive()) {
                        cellHealth = cellHealth - (counts[1][i][j] - 2);
                    } else {
                        cellHealth = cellHealth - (counts[1][i][j] - 1);
                    }

                    // Boundaries for cell health
                    if (cellHealth < 0) {
                        cellHealth = 0;
                    }
                    if (cellHealth > 100) {
                        cellHealth = 100;
                    }

                    cell.setCellHealth(cellHealth);

                } // Change cancerous cell health to 0 appropriately
                else {
                    if (cell.isDrugActive()) {
                        cell.setAggressive(false);
                    }
                    cell.setCellHealth(0);
                    cancerCount++;
                }

                // Ensure cell is not absorbing the drug, but it can pass by the cell
                if (cell.getCellType() == 2) {
                    if (!uniform) {
                        cell.setInterstitialConc(c1changes[i][j]);
                        cell.setIntracellularFree(c2changes[i][j]);
                        cell.setIntracellularBound(0.0);
                    }

                    cell.setDrugActive(false);
                }


                j += 1;
            }
            i += 1;
            j = 0;
        }
    }

    // Modes of cancer cell spread, and the second layer of cell updating
    public void SpreadCancer() {
        GetNeighbourCounts();
        boolean cancerous = false;
        for (int i = 0; i < allCells.length; i++) {
            for (int j = 0; j < allCells[i].length; j++) {
                currentCell = allCells[i][j];
                if (currentCell.getCellType() != 1) {
                    if (random) {
                        cancerous = DetermineCancerousRandomly(i, j);
                    } else {
                        cancerous = DetermineCancerousDeterministically(i, j);
                    }

                }


                // kill cell
                if (cancerous == true) {
                    if (!uniform) {
                        // Reset the internal concentrations of the drug
                        currentCell.setIntracellularFree(0.0);
                        currentCell.setIntracellularBound(0.0);
                        currentCell.setDrugActive(false);
                        currentCell.setAggressive(true);
                    } else {
                        // The drug is always fully absorbed, so uniform cancerous
                        // cells are always passive
                        currentCell.setAggressive(false);
                    }
                    currentCell.setCellHealth(0);
                    cancerCount++;
                    allCells[i][j].setCellType(1);
                    neighbourMatrix[i + 1][j + 1] = allCells[i][j];
                    startGrid[i][j].setBackground(Color.ORANGE);
                    labels[i][j].setForeground(Color.BLACK);
                }

                cancerous = false;

                if (currentCell.getCellType() == 1) {
                    labels[i][j].setForeground(Color.BLACK);
                    startGrid[i][j].setBackground(Color.ORANGE);
                }

                healthString = "";
                if (healthText == true) {
                    if (currentCell.getCellType() != 1) {
                        healthString = String.valueOf(currentCell.getCellHealth());
                    } else {
                        if (currentCell.isAggressive()) {
                            healthString = "Aggressive";
                        } else {
                            healthString = "Passive";
                        }
                    }
                    labels[i][j].setText(healthString);
                } else {
                    if (currentCell.getCellType() != 2) {
                        if (currentCell.getAbsorptionValue() > 1) {
                            labels[i][j].setText(String.format("%3.1f", currentCell.getAbsorptionValue()));
                        } else {
                            labels[i][j].setText(String.format("%6.1e", currentCell.getAbsorptionValue()));
                        }
                    } else {
                        labels[i][j].setText("0");
                    }
                }
            }
        }
    }

    // Determining if a cell is cancerous randomly
    public boolean DetermineCancerousRandomly(int i, int j) {
        boolean cancerous = false;
        int cellHealth = currentCell.getCellHealth();
        int neighbourAgg = neighbourAggressions[i][j];
        int neighbourCount = counts[1][i][j];
        double chanceOfSpread = 0.0;

        if (neighbourCount == 0) {
            // Cell can't spread
            chanceOfSpread = 0;
        }

        // Else determine likelihood of survival
        else if (neighbourCount <= 7) {
            chanceOfSpread = neighbourAgg + (0.2*(neighbourCount-neighbourAgg));
            chanceOfSpread *= (100 - cellHealth) / 100;
            chanceOfSpread /= 4;
        }

        // If it is surrounded by 8 neighbours, it has a 75% chance of
        // its health as a decimal of becoming cancerous
        else if (neighbourCount == 8) {
            chanceOfSpread = 0.75 * ((100 - currentCell.getCellHealth()) / 100);
        }

        // Determine if the cell survives based on chanceOfSpread
        if (chanceOfSpread >= Math.random()) {
            cancerous = true;
        }
        return cancerous;
    }

    // Determining if a cell is cancerous deterministically
    public boolean DetermineCancerousDeterministically(int i, int j) {
        boolean cancerous = false;
        int cellHealth = currentCell.getCellHealth();
        int neighbourAgg = neighbourAggressions[i][j];
        int neighbourCount = counts[1][i][j];
        if (neighbourCount <= 6) {
            // 0.2 * passive neighbour count, add 1 * aggressive neighbour count
            // i.e. cancer cells where the drug is above the threshold are 5 times
            // less aggressive than ordinary cancerous cells
            if (cellHealth < ((0.2*(neighbourCount - neighbourAgg)) + neighbourAgg)) {
                cancerous = true;
            }
        } else if (neighbourCount == 7) {
            if (cellHealth < ((0.2*(neighbourCount - neighbourAgg)) + neighbourAgg*2)) {
                cancerous = true;
            }

        } else if (neighbourCount == 8) {
            if (cellHealth < (2*(0.2*(neighbourCount - neighbourAgg)) + neighbourAgg)) {
                cancerous = true;
            }
        }
        return cancerous;
    }

}
