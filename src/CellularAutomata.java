import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/*
 * Created on 15th January 2018
 * By Jordan Day
 * GUI code and input handling for initial screen
 */
public class CellularAutomata extends JFrame {

    JSpinner gridSizeSpinner;
    JSpinner seededSpinner;
    JSpinner stepTime;
    JSpinner generations;
    JButton startButton;
    JButton variablesButton;
    int gridSize;

    String mode = "";
    Cell[][] allCells;

    public CellularAutomata() {}


    // Initialises all window components, and positions them correctly on the screen
    public void InitialiseWindow() {

        // Label setups with text
        JLabel gridSizeLbl = new JLabel("Number of columns/ rows: ");
        JLabel genLbl = new JLabel("Mode of cell types generation: ");
        JLabel seedBtnLbl = new JLabel("Seed Value (if applicable): ");
        JLabel drugModeLbl = new JLabel("Drug Introduction Model: ");
        JLabel cancerModeLbl = new JLabel("Cancer Spread Mode: ");
        JLabel stepTimeLbl = new JLabel("Time for each generation to refresh: ");
        JLabel generationsLbl = new JLabel("Number of iterations: ");

        SpinnerModel sizeVal = new SpinnerNumberModel(10, 10, 100, 10);
        gridSizeSpinner = new JSpinner(sizeVal);

        // Creating a ButtonGroup for the mode of setting up the tissue's cells
        JRadioButton randomBtn = new JRadioButton("Random");
        JRadioButton seededBtn = new JRadioButton("Random seeded");
        JRadioButton selfSelectBtn = new JRadioButton("User-selected");
        ButtonGroup genOptions = new ButtonGroup();
        genOptions.add(randomBtn);
        genOptions.add(seededBtn);
        genOptions.add(selfSelectBtn);
        randomBtn.setSelected(true);

        SpinnerModel seedVal = new SpinnerNumberModel(0, 0, 999999999, 1);
        seededSpinner = new JSpinner(seedVal);

        // Creating a ButtonGroup for the mode of drug introduction
        JRadioButton uniformBtn = new JRadioButton("Uniform");
        JRadioButton diffusionBtn = new JRadioButton("Diffusion (from boundaries)");
        ButtonGroup drugOptions = new ButtonGroup();
        drugOptions.add(uniformBtn);
        drugOptions.add(diffusionBtn);
        uniformBtn.setSelected(true);

        // Creating a ButtonGroup for the mode of cancer spread
        JRadioButton probabilityBtn = new JRadioButton("Probability");
        JRadioButton deterministicBtn = new JRadioButton("Deterministic");
        ButtonGroup cancerOptions = new ButtonGroup();
        cancerOptions.add(probabilityBtn);
        cancerOptions.add(deterministicBtn);
        probabilityBtn.setSelected(true);

        SpinnerModel timeVal = new SpinnerNumberModel(5, 5, 20, 1);
        stepTime = new JSpinner(timeVal);

        SpinnerModel genVal = new SpinnerNumberModel(100, 10, 500, 1);
        generations = new JSpinner(genVal);

        startButton = new JButton("Begin Automaton");
        startButton.setToolTipText("Enters the cell setup screen");

        // JButton's can understand html, so that is used here for a newline character
        variablesButton = new JButton("<html>Change default values<br />for drug diffusion</html>");
        variablesButton.setToolTipText("Enters the diffusion model values screen");

        // GridBagLayout acts as a grid to store components in particular cells
        GridBagLayout initialGrid = new GridBagLayout();
        this.setLayout(initialGrid);

        // GridBagConstraints allows us to manipulate each component within the grid, allowing us
        // to change things such as the amount of cells it occupies
        GridBagConstraints initialConstraints = new GridBagConstraints();
        initialConstraints.insets = new Insets(10, 20, 20, 20);

        initialConstraints.weighty = 0.5;
        initialConstraints.weightx = 0.5;
        initialConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialConstraints.gridx = 0;
        initialConstraints.gridy = 0;
        initialConstraints.anchor = GridBagConstraints.CENTER;
        initialConstraints.gridwidth = 2;
        this.add(gridSizeLbl, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 0;
        initialConstraints.gridwidth = 1;
        this.add(gridSizeSpinner, initialConstraints);


        initialConstraints.gridx = 1;
        initialConstraints.gridy = 1;
        initialConstraints.weighty = 0.2;
        this.add(genLbl, initialConstraints);


        initialConstraints.gridx = 0;
        initialConstraints.gridy = 2;
        initialConstraints.weighty = 0.5;
        this.add(randomBtn, initialConstraints);

        initialConstraints.gridx = 1;
        initialConstraints.gridy = 2;
        this.add(seededBtn, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 2;
        this.add(selfSelectBtn, initialConstraints);


        initialConstraints.gridx = 0;
        initialConstraints.gridy = 3;
        initialConstraints.gridwidth = 2;
        this.add(seedBtnLbl, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 3;
        initialConstraints.gridwidth = 1;
        this.add(seededSpinner, initialConstraints);


        initialConstraints.gridx = 1;
        initialConstraints.gridy = 4;
        initialConstraints.weighty = 0.2;
        this.add(drugModeLbl, initialConstraints);


        initialConstraints.gridx = 0;
        initialConstraints.gridy = 5;
        initialConstraints.weighty = 0.5;
        this.add(uniformBtn, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 5;
        this.add(diffusionBtn, initialConstraints);


        initialConstraints.gridx = 1;
        initialConstraints.gridy = 6;
        initialConstraints.weighty = 0.2;
        this.add(cancerModeLbl, initialConstraints);


        initialConstraints.gridx = 0;
        initialConstraints.gridy = 7;
        initialConstraints.weighty = 0.5;
        this.add(probabilityBtn, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 7;
        this.add(deterministicBtn, initialConstraints);


        initialConstraints.gridx = 0;
        initialConstraints.gridy = 8;
        this.add(stepTimeLbl, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 8;
        initialConstraints.gridwidth = 1;
        this.add(stepTime, initialConstraints);


        initialConstraints.gridx = 0;
        initialConstraints.gridy = 9;
        initialConstraints.gridwidth = 2;
        this.add(generationsLbl, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 9;
        initialConstraints.gridwidth = 1;
        this.add(generations, initialConstraints);


        initialConstraints.gridx = 0;
        initialConstraints.gridy = 10;
        this.add(startButton, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 10;
        this.add(variablesButton, initialConstraints);


        // Telling the user what colour means what cell type
        JLabel type0 = new JLabel("<html><font color='white'>Healthy cell</font></html>\"));");
        JPanel healthy = new JPanel();
        healthy.setBackground(Color.BLUE);
        healthy.add(type0);
        JLabel type1 = new JLabel("Cancerous cell");
        JPanel cancerous = new JPanel();
        cancerous.setBackground(Color.ORANGE);
        cancerous.add(type1);
        JLabel type2 = new JLabel("<html><font color='white'>Empty cell</font></html>\"));");
        JPanel empty = new JPanel();
        empty.setBackground(Color.BLACK);
        empty.add(type2);

        initialConstraints.gridx = 0;
        initialConstraints.gridy = 11;
        this.add(healthy, initialConstraints);

        initialConstraints.gridx = 1;
        initialConstraints.gridy = 11;
        this.add(cancerous, initialConstraints);

        initialConstraints.gridx = 2;
        initialConstraints.gridy = 11;
        this.add(empty, initialConstraints);



        // The ActionListener for the start button.
        // Allows us to decide on which variables to pass to which screen based on selections
        startButton.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed (ActionEvent e) {
                gridSize = (Integer) gridSizeSpinner.getValue();

                // If the user selected the "Random" mode for cell selection
                if (genOptions.getSelection().equals(randomBtn.getModel())) {
                    mode = "Random";

                    // Go straight to the CellSimulation class
                    allCells = new Cell[gridSize][gridSize];
                    CellSimulation simulation = new CellSimulation();
                    simulation.setGridSize(gridSize);
                    simulation.setStepTime((Integer) stepTime.getValue());
                    simulation.setGenerations((Integer) generations.getValue());

                    // Close the CellularAutomata class window
                    dispose();

                    // Sets whether the user wants the Uniform or Diffusion
                    // drug introduction model
                    if (drugOptions.getSelection().equals(uniformBtn.getModel())) {
                        simulation.setUniform(true);
                    } else {
                        simulation.setUniform(false);
                    }

                    // Sets whether the user wants the Probability or Deterministic
                    // cancer spreading model
                    if (cancerOptions.getSelection().equals(probabilityBtn.getModel())) {
                        simulation.setRandom(true);
                    } else {
                        simulation.setRandom(false);
                    }

                    // Populates the grid with a random 2D array of cell types
                    for (int row = 0; row < gridSize; row++) {
                        for (int col = 0; col < gridSize; col++) {
                            allCells[row][col] = new Cell();

                            double randNumber = (Math.random() * 3);
                            int type = (int) Math.floor(randNumber);
                            allCells[row][col].setCellType(type);
                        }
                    }

                    simulation.setAllCells(allCells);
                    simulation.CreateWindow();
                    simulation.setVisible(true);
                    simulation.StepThroughGenerations();

                    // If the user selected the "Random Seeded" mode for cell selection
                } else if (genOptions.getSelection().equals(seededBtn.getModel())) {
                    mode = "Seeded";

                    // Go straight to the CellSimulation class
                    allCells = new Cell[gridSize][gridSize];
                    CellSimulation simulation = new CellSimulation();
                    simulation.setGridSize(gridSize);
                    simulation.setStepTime((Integer) stepTime.getValue());
                    simulation.setGenerations((Integer) generations.getValue());

                    dispose();

                    // Sets whether the user wants the Uniform or Diffusion
                    // drug introduction model
                    if (drugOptions.getSelection().equals(uniformBtn.getModel())) {
                        simulation.setUniform(true);
                    } else {
                        simulation.setUniform(false);
                    }

                    // Sets whether the user wants the Probability or Deterministic
                    // cancer spreading model
                    if (cancerOptions.getSelection().equals(probabilityBtn.getModel())) {
                        simulation.setRandom(true);
                    } else {
                        simulation.setRandom(false);
                    }

                    // Uses the seed value the user has provided
                    int seed = (Integer) seededSpinner.getValue();
                    Random randomSeeded = new Random(seed);

                    // Populates the grid with a random seeded 2D array of cell types
                    for (int row = 0; row < gridSize; row++) {
                        for (int col = 0; col < gridSize; col++) {
                            allCells[row][col] = new Cell();

                            int randNumber = randomSeeded.nextInt(3);
                            allCells[row][col].setCellType(randNumber);
                        }
                    }

                    simulation.setAllCells(allCells);
                    simulation.CreateWindow();
                    simulation.setVisible(true);
                    simulation.StepThroughGenerations();

                    // If the user selected the "User-selected" mode for cell selection
                } else if (genOptions.getSelection().equals(selfSelectBtn.getModel())) {
                    mode = "User";

                    // Go to the CellGUI class for the user to select cell types
                    CellGUI gui = new CellGUI();
                    gui.setGridSize(gridSize);
                    gui.setStepTime((Integer) stepTime.getValue());
                    gui.setGenerations((Integer) generations.getValue());

                    dispose();

                    // Sets whether the user wants the Uniform or Diffusion
                    // drug introduction model
                    if (drugOptions.getSelection().equals(uniformBtn.getModel())) {
                        gui.setDrugMode("Uniform");
                    } else {
                        gui.setDrugMode("Diffusion");
                    }

                    // Sets whether the user wants the Probability or Deterministic
                    // cancer spreading model
                    if (cancerOptions.getSelection().equals(probabilityBtn.getModel())) {
                        gui.setCancerMode("Probability");
                    } else {
                        gui.setCancerMode("Deterministic");
                    }

                    gui.CreateWindow();
                    gui.setVisible(true);
                }
            }
        });

        // The ActionListener for the diffusion screen button.
        // Allows us to decide on which values to pass to diffusion screen based on selections
        variablesButton.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent e) {
                gridSize = (Integer) gridSizeSpinner.getValue();
                // Go straight to the DiffusionScreen class
                allCells = new Cell[gridSize][gridSize];
                DiffusionScreen difScrn = new DiffusionScreen();
                difScrn.setGridSize(gridSize);
                difScrn.setStepTime((Integer) stepTime.getValue());
                difScrn.setGenerations((Integer) generations.getValue());

                dispose();

                // If the user selected the "Random" mode for cell selection
                if (genOptions.getSelection().equals(randomBtn.getModel())) {
                    difScrn.setGenMode("Random");
                    // Populates the grid with a random 2D array of cell types
                    for (int row = 0; row < gridSize; row++) {
                        for (int col = 0; col < gridSize; col++) {
                            allCells[row][col] = new Cell();

                            double randNumber = (Math.random() * 3);
                            int type = (int) Math.floor(randNumber);
                            allCells[row][col].setCellType(type);
                        }
                    }
                    difScrn.setAllCells(allCells);

                    // If the user selected the "Random Seeded" mode for cell selection
                } else if (genOptions.getSelection().equals(seededBtn.getModel())) {
                    difScrn.setGenMode("Seeded");
                    difScrn.setSeed((Integer) seededSpinner.getValue());

                    // Uses the seed value the user has provided
                    int seed = (Integer) seededSpinner.getValue();
                    Random randomSeeded = new Random(seed);

                    // Populates the grid with a random seeded 2D array of cell types
                    for (int row = 0; row < gridSize; row++) {
                        for (int col = 0; col < gridSize; col++) {
                            allCells[row][col] = new Cell();

                            int randNumber = randomSeeded.nextInt(3);
                            allCells[row][col].setCellType(randNumber);
                        }
                    }
                    difScrn.setAllCells(allCells);

                    // If the user selected the "User-selected" mode for cell selection
                } else if (genOptions.getSelection().equals(selfSelectBtn.getModel())) {
                    difScrn.setGenMode("User");
                }



                // Sets whether the user wants the Uniform or Diffusion
                // drug introduction model
                if (drugOptions.getSelection().equals(uniformBtn.getModel())) {
                    difScrn.setDrugMode("Uniform");
                } else if (drugOptions.getSelection().equals(diffusionBtn.getModel())) {
                    difScrn.setDrugMode("Diffusion");
                }

                // Sets whether the user wants the Probability or Deterministic
                // cancer spreading model
                if (cancerOptions.getSelection().equals(probabilityBtn.getModel())) {
                    difScrn.setCancerMode("Probability");
                } else if (cancerOptions.getSelection().equals(deterministicBtn.getModel())) {
                    difScrn.setCancerMode("Deterministic");
                }


                difScrn.CreateWindow();
                difScrn.setVisible(true);

            }
        });

        // Window features such as size and how to handle exiting the window
        this.setSize(new Dimension(800, 600));
        this.setMinimumSize(this.getSize());
        setTitle("Cellular Automaton GUI");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }
}
