import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Random;

/*
 * Created on 15th January 2018
 * By Jordan Day
 * GUI code and input handling for initial screen
 */
public class DiffusionScreen extends JFrame {

    JSpinner cellPermeability;
    JSpinner membranePermeability;
    JSpinner drugAssociation;
    JSpinner drugDissociation;
    JSpinner timeStep;
    JSpinner bindingThreshold;
    JButton startButton;

    int gridSize;

    Cell[][] allCells;

    int seed;
    int stepTime;
    int generations;

    String genMode = "";
    String drugMode = "";
    String cancerMode = "";

    // Setter methods for variables being passed from CellularAutomata
    public void setGenerations(int generations) {
        this.generations = generations;
    }

    public void setGridSize(int gridSize) { this.gridSize = gridSize; }

    public void setSeed(int seed) {
        this.seed = seed;
    }

    public void setStepTime(int stepTime) {
        this.stepTime = stepTime;
    }

    public void setGenMode(String genMode) {
        this.genMode = genMode;
    }

    public void setDrugMode(String drugMode) {
        this.drugMode = drugMode;
    }

    public void setCancerMode(String cancerMode) {
        this.cancerMode = cancerMode;
    }

    public void setAllCells(Cell[][] allCells) {
        this.allCells = allCells;
    }

    public DiffusionScreen() {}

    // Initialises all window components, and positions them correctly on the screen
    public void InitialiseWindow() {

        // Label setups with text
        JLabel cellPermLbl = new JLabel("Permeability between cells [k0]: ");
        JLabel membrPermLbl = new JLabel("Permeability across cell membrane [k1]: ");
        JLabel drugAssocLbl = new JLabel("Drug association rate [k2]: ");
        JLabel drugDissocLbl = new JLabel("Drug dissociation rate [k-2]: ");
        JLabel timeStepLbl = new JLabel("Timestep [dt]: ");
        JLabel bindingThresLbl = new JLabel("Threshold of drug concentration bound to be 'active' in cell [1 x 10^-n] : ");


        SpinnerModel cellPermVal = new SpinnerNumberModel(2.5, 0.2, 40, 0.1);
        cellPermeability = new JSpinner(cellPermVal);

        SpinnerModel membrPermVal = new SpinnerNumberModel(1.0, 0.1, 30, 0.1);
        membranePermeability = new JSpinner(membrPermVal);

        SpinnerModel drugAssocVal = new SpinnerNumberModel(0.9, 0.1, 40, 0.1);
        drugAssociation = new JSpinner(drugAssocVal);

        SpinnerModel drugDissocVal = new SpinnerNumberModel(14, 2, 50, 0.1);
        drugDissociation = new JSpinner(drugDissocVal);

        SpinnerModel timeStepVal = new SpinnerNumberModel(1, 0.01, 1, 0.01);
        timeStep = new JSpinner(timeStepVal);

        SpinnerModel bindingThresVal = new SpinnerNumberModel(30, 0, 30,0.5);
        bindingThreshold = new JSpinner(bindingThresVal);

        JLabel cellPermUnit = new JLabel(" x 10^-6 m s^-1");
        JLabel membrPermUnit = new JLabel(" x 10^-6 m s^-1");
        JLabel drugAssocUnit = new JLabel(" x 10^-6 μ M^-1 s^-1");
        JLabel drugDissocUnit = new JLabel(" x 10^-5 s^-1");
        JLabel timeStepUnit = new JLabel(" seconds");
        JLabel bindingThresUnit = new JLabel(" μ M");


        startButton = new JButton("Begin Automaton");
        startButton.setToolTipText("Enters the cell setup/ cell simulation screen");

        // GridBagLayout acts as a grid to store components in particular cells
        GridBagLayout initialGrid = new GridBagLayout();
        this.setLayout(initialGrid);

        // GridBagConstraints allows us to manipulate each component within the grid, allowing us
        // to change things such as the amount of cells it occupies
        GridBagConstraints initialConstraints = new GridBagConstraints();
        initialConstraints.insets = new Insets(10, 20, 20, 20);

        initialConstraints.weighty = 0.5;
        initialConstraints.weightx = 0.5;
        initialConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialConstraints.gridx = 0;
        initialConstraints.gridy = 0;
        initialConstraints.anchor = GridBagConstraints.CENTER;
        this.add(cellPermLbl, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 1;
        initialConstraints.gridy = 0;
        this.add(cellPermeability, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 2;
        initialConstraints.gridy = 0;
        this.add(cellPermUnit, initialConstraints);


        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 0;
        initialConstraints.gridy = 1;
        this.add(membrPermLbl, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 1;
        initialConstraints.gridy = 1;
        this.add(membranePermeability, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 2;
        initialConstraints.gridy = 1;
        this.add(membrPermUnit, initialConstraints);


        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 0;
        initialConstraints.gridy = 2;
        this.add(drugAssocLbl, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 1;
        initialConstraints.gridy = 2;
        this.add(drugAssociation, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 2;
        initialConstraints.gridy = 2;
        this.add(drugAssocUnit, initialConstraints);


        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 0;
        initialConstraints.gridy = 3;
        this.add(drugDissocLbl, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 1;
        initialConstraints.gridy = 3;
        this.add(drugDissociation, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 2;
        initialConstraints.gridy = 3;
        this.add(drugDissocUnit, initialConstraints);


        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 0;
        initialConstraints.gridy = 4;
        this.add(timeStepLbl, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 1;
        initialConstraints.gridy = 4;
        this.add(timeStep, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 2;
        initialConstraints.gridy = 4;
        this.add(timeStepUnit, initialConstraints);


        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 0;
        initialConstraints.gridy = 5;
        this.add(bindingThresLbl, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 1;
        initialConstraints.gridy = 5;
        this.add(bindingThreshold, initialConstraints);

        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 2;
        initialConstraints.gridy = 5;
        this.add(bindingThresUnit, initialConstraints);


        initialConstraints.weighty = 0.5;
        initialConstraints.gridx = 0;
        initialConstraints.gridy = 6;
        initialConstraints.gridwidth = 3;
        this.add(startButton, initialConstraints);


        // The ActionListener for the start button.
        // Allows us to decide on which variables to pass to which screen based on selections
        startButton.addActionListener(new ActionListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void actionPerformed(ActionEvent e) {

                // If the user selected the "Random" mode for cell selection
                if (genMode == "Random") {

                    // Initialise CellSimulation class and pass variables through
                    CellSimulation simulation = new CellSimulation();
                    simulation.setGridSize(gridSize);
                    simulation.setStepTime(stepTime);
                    simulation.setGenerations(generations);

                    dispose();
                    if (drugMode == "Uniform") {
                        simulation.setUniform(true);
                    } else {
                        // Set custom values for drug diffusion variables
                        simulation.setUniform(false);
                        double customCellPerm = (Double) cellPermeability.getValue();
                        simulation.setCellPermeability(customCellPerm / 1000000.0);
                        double customMembrPerm = (Double) membranePermeability.getValue();
                        simulation.setMembranePermeability(customMembrPerm / 1000000.0);
                        double customDrugAssoc = (Double) drugAssociation.getValue();
                        simulation.setAssociationRate(customDrugAssoc / 1000000000000.0);
                        double customDrugDissoc = (Double) drugDissociation.getValue();
                        simulation.setDissociationRate(customDrugDissoc / 100000.0);
                        double customTimeStep = (Double) timeStep.getValue();
                        simulation.setTimeStep(customTimeStep);
                        double customBindingThres = (Double) bindingThreshold.getValue();
                        simulation.setBindingThreshold(Math.pow(10, -customBindingThres));

                    }

                    if (cancerMode == "Probability") {
                        simulation.setRandom(true);
                    } else if (cancerMode == "Deterministic") {
                        simulation.setRandom(false);
                    }

                    simulation.setAllCells(allCells);
                    simulation.CreateWindow();
                    simulation.setVisible(true);
                    // wait?
                    simulation.StepThroughGenerations();

                    // If the user selected the "Random Seeded" mode for cell selection
                } else if (genMode == "Seeded") {

                    // Initialise CellSimulation class and pass variables through
                    CellSimulation simulation = new CellSimulation();
                    simulation.setGridSize(gridSize);
                    simulation.setStepTime(stepTime);
                    simulation.setGenerations(generations);

                    dispose();
                    if (drugMode == "Uniform") {
                        simulation.setUniform(true);
                    } else {
                        // Set custom values for drug diffusion variables
                        simulation.setUniform(false);
                        double customCellPerm = (Double) cellPermeability.getValue();
                        simulation.setCellPermeability(customCellPerm / 1000000.0);
                        double customMembrPerm = (Double) membranePermeability.getValue();
                        simulation.setMembranePermeability(customMembrPerm / 1000000.0);
                        double customDrugAssoc = (Double) drugAssociation.getValue();
                        simulation.setAssociationRate(customDrugAssoc / 1000000000000.0);
                        double customDrugDissoc = (Double) drugDissociation.getValue();
                        simulation.setDissociationRate(customDrugDissoc / 100000.0);
                        double customTimeStep = (Double) timeStep.getValue();
                        simulation.setTimeStep(customTimeStep);
                        double customBindingThres = (Double) bindingThreshold.getValue();
                        simulation.setBindingThreshold(Math.pow(10, -customBindingThres));
                    }

                    if (cancerMode == "Probability") {
                        simulation.setRandom(true);
                    } else if (cancerMode == "Deterministic") {
                        simulation.setRandom(false);
                    }

                    simulation.setAllCells(allCells);
                    simulation.CreateWindow();
                    simulation.setVisible(true);
                    // wait?
                    simulation.StepThroughGenerations();

                    // If the user selected the "User Select" mode for cell selection
                } else if (genMode == "User") {

                    // Initialise CellSimulation class and pass variables through
                    CellGUI gui = new CellGUI();
                    gui.setGridSize(gridSize);
                    gui.setStepTime(stepTime);
                    gui.setGenerations(generations);
                    gui.setDrugMode(drugMode);
                    gui.setCancerMode(cancerMode);

                    // Set custom values for drug diffusion variables
                    double customCellPerm = (Double) cellPermeability.getValue();
                    gui.setCellPermeability(customCellPerm / 1000000.0);
                    double customMembrPerm = (Double) membranePermeability.getValue();
                    gui.setMembranePermeability(customMembrPerm / 1000000.0);
                    double customDrugAssoc = (Double) drugAssociation.getValue();
                    gui.setAssociationRate(customDrugAssoc / 1000000000000.0);
                    double customDrugDissoc = (Double) drugDissociation.getValue();
                    gui.setDissociationRate(customDrugDissoc / 100000.0);
                    double customTimeStep= (Double) timeStep.getValue();
                    gui.setTimeStep(customTimeStep);
                    double customBindingThres = (Double) bindingThreshold.getValue();
                    gui.setBindingThreshold(Math.pow(10, -customBindingThres));

                    dispose();
                    gui.CreateWindow();
                    gui.setVisible(true);
                }
            }
        });
    }

    // Create the window that has been set up previously
    public void CreateWindow() {
        InitialiseWindow();

        // Window features such as size and how to handle exiting the window
        this.setSize(new Dimension(800, 600));
        this.setMinimumSize(this.getSize());
        setTitle("Drug Values Screen");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}

