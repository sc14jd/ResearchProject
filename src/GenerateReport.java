import org.knowm.xchart.*;
import org.knowm.xchart.BitmapEncoder.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

/*
 * Created on 16th January 2018
 * By Jordan Day
 * Analysis tool code to display simulation statistics to the user
 */
public class GenerateReport extends JFrame {


    int gridSize;
    boolean uniform;
    boolean random;
    double[] xCoords;
    double[] cancerCounts;
    double[] absorbedCounts;
    double[] totalAbsorptions;
    double cellPermeability;
    double membranePermeability;
    double associationRate;
    double dissociationRate;
    double timeStep;
    double bindingThreshold;

    static XYChart spreadChart;
    static XYChart percentageChart;
    static XYChart drugSpreadChart;
    static XYChart drugTotalChart;


    public void setGridSize(int gridSize) { this.gridSize = gridSize; }

    public void setUniform(boolean uniform) {
        this.uniform = uniform;
    }

    public void setRandom(boolean random) {
        this.random = random;
    }

    // Also creates array for the x coordinates
    public void setCancerCounts(double[] cancerCounts) {
        this.cancerCounts = cancerCounts;
        xCoords = new double[cancerCounts.length];
        for (int i = 0; i < xCoords.length; ++i) {
            xCoords[i] = i+1;
        }
    }

    public void setAbsorbedCounts(double[] absorbedCounts) {
        this.absorbedCounts = absorbedCounts;
    }

    public void setTotalAbsorptions(double[] totalAbsorptions) {
        this.totalAbsorptions = totalAbsorptions;
    }

    public void setCellPermeability(double cellPermeability) {
        this.cellPermeability = cellPermeability;
    }

    public void setMembranePermeability(double membranePermeability) {
        this.membranePermeability = membranePermeability;
    }

    public void setAssociationRate(double associationRate) {
        this.associationRate = associationRate;
    }

    public void setDissociationRate(double dissociationRate) {
        this.dissociationRate = dissociationRate;
    }

    public void setTimeStep(double timeStep) {
        this.timeStep = timeStep;
    }

    public void setBindingThreshold(double bindingThreshold) {
        this.bindingThreshold = bindingThreshold;
    }

    public GenerateReport() {
    }


    // Creates the chart which shows how many cancer cells were present at each generation
    public XYChart ChartCancerSpread() {
        System.out.println("Charting cancer spread");
        spreadChart = QuickChart.getChart("Amount of Cancer Cells at Each Generation",
                "Generation", "Cancer Cell Count",
                "y(x)", xCoords, cancerCounts);
        return spreadChart;
        //new SwingWrapper(spreadChart).displayChart();
    }

    // Creates the chart which shows how many cancer cells as a percentage of total cells
    // were present at each generation
    public XYChart ChartCellPercentages() {

        int cellCount = (gridSize*gridSize);
        String percentageRoundedStr;
        // Logic for generating the values as percentages
        double[] percentages = new double[cancerCounts.length];
        for (int i = 0; i < percentages.length; i++) {
            percentageRoundedStr = String.format("%3.5f", (cancerCounts[i] / cellCount) * 100);
            percentages[i] = Double.valueOf(percentageRoundedStr);
        }

        percentageChart = QuickChart.getChart("Percentage of Cancer Cells in Tissue at Each Generation",
                "Generation", "Cancer Cell Count %",
                "y(x)", xCoords, percentages);
        return percentageChart;
        //new SwingWrapper(percentageChart).displayChart();
    }


    // Similar to ChartCancerSpread, but for number of cells that have absorbed the drug at a given generation
    public XYChart ChartDrugSpread() {

        int cellCount = (gridSize*gridSize);
        String percentageRoundedStr;
        // Logic for generating the values as percentages
        double[] percentages = new double[absorbedCounts.length];
        for (int i = 0; i < percentages.length; i++) {
            percentageRoundedStr = String.format("%3.5f", (absorbedCounts[i] / cellCount) * 100);
            percentages[i] = Double.valueOf(percentageRoundedStr);
        }

        drugSpreadChart = QuickChart.getChart("Percentage of Cells Where the Bound Drug is Active",
                "Generation", "Cell Count %",
                "y(x)", xCoords, percentages);
        return drugSpreadChart;
    }

    // Shows the total amount of drug absorption across all cells over the tissue
    public XYChart ChartTotalDrugAbsorption() {

        drugTotalChart = QuickChart.getChart("Sum of all Values of Absorbed Cell Binding Concentrations",
                "Generation", "Absorption Count",
                "y(x)", xCoords, totalAbsorptions);
        return drugTotalChart;

    }

    // Primary method of this class, that creates the window and places every
    // element in the window in the right place
    public void CreateReportWindow() {
        // Create and set up the window.
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setPreferredSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width-20, Toolkit.getDefaultToolkit().getScreenSize().height-80));

        this.setMinimumSize(this.getPreferredSize());


        // Container for all charts
        JPanel mainContainer = new JPanel();
        mainContainer.setLayout(new BoxLayout(mainContainer, BoxLayout.Y_AXIS));

        // Container for generic cancer spread charts
        JPanel topContainer = new JPanel();
        topContainer.setLayout(new BoxLayout(topContainer, BoxLayout.X_AXIS));
        JPanel cancerSpreadPanel = new XChartPanel<>(ChartCancerSpread());
        topContainer.add(cancerSpreadPanel, BorderLayout.CENTER);
        JPanel cancerPercentPanel = new XChartPanel<>(ChartCellPercentages());
        topContainer.add(cancerPercentPanel, BorderLayout.CENTER);

        mainContainer.add(topContainer, BorderLayout.CENTER);

        // We only show drug charts for the drug diffusion model, i.e. when they are relevant
        if (!uniform) {

            // Container for drug diffusion charts
            JPanel bottomContainer = new JPanel();
            bottomContainer.setLayout(new BoxLayout(bottomContainer, BoxLayout.X_AXIS));

            JPanel drugSpreadPanel = new XChartPanel<>(ChartDrugSpread());
            bottomContainer.add(drugSpreadPanel, BorderLayout.CENTER);
            JPanel drugTotalPanel = new XChartPanel<>(ChartTotalDrugAbsorption());
            bottomContainer.add(drugTotalPanel, BorderLayout.CENTER);

            mainContainer.add(bottomContainer);
        }

        // Labels for the report
        JLabel gridSizeLbl = new JLabel("  Grid Size: (" + gridSize + " x " + gridSize + ")");
        JLabel drugModeLbl;
        if (uniform == true) {
            drugModeLbl = new JLabel("  Drug Introduction Mode: Uniform");
        } else {
            drugModeLbl = new JLabel("  Drug Introduction Mode: Diffusion");
        }

        JLabel cancerModeLbl;
        if (random == true){
            cancerModeLbl = new JLabel("  Cancer Spread Mode: Random");
        } else {
            cancerModeLbl = new JLabel("  Cancer Spread Mode: Deterministic");
        }

        JPanel labelsContainer = new JPanel();
        labelsContainer.setLayout(new BoxLayout(labelsContainer, BoxLayout.X_AXIS));
        labelsContainer.add(gridSizeLbl);
        labelsContainer.add(new JLabel("                   "));
        labelsContainer.add(drugModeLbl);
        labelsContainer.add(new JLabel("                   "));
        labelsContainer.add(cancerModeLbl);

        mainContainer.add(labelsContainer);

        if (!uniform) {
            JPanel drugLabelsContainer = new JPanel();
            JLabel k0Lbl = new JLabel("    k0: " + String.format("%3.2e", cellPermeability) + " m s^-1");
            JLabel k1Lbl = new JLabel("    k1: " + String.format("%3.2e", membranePermeability) + " m s^-1");
            JLabel k2Lbl = new JLabel("    k2: " + String.format("%3.2e", associationRate) + " M^-1 s^-1");
            JLabel k_2Lbl = new JLabel("    k-2: " + String.format("%3.2e", dissociationRate) + " s^-1");
            JLabel timeStepLbl = new JLabel("    Time step: " + timeStep + " seconds");
            JLabel bindingThresLbl = new JLabel("    Binding Threshold: " + bindingThreshold + " M");
            drugLabelsContainer.add(k0Lbl);
            drugLabelsContainer.add(k1Lbl);
            drugLabelsContainer.add(k2Lbl);
            drugLabelsContainer.add(k_2Lbl);
            drugLabelsContainer.add(timeStepLbl);
            drugLabelsContainer.add(bindingThresLbl);
            mainContainer.add(drugLabelsContainer);
        }


        JPanel textContainer = new JPanel();
        textContainer.setLayout(new BoxLayout(textContainer, BoxLayout.X_AXIS));
        JLabel textLbl = new JLabel("  Prefix for chart filenames:                   ");
        JTextField saveString = new JTextField();
        saveString.setMaximumSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width-20, 40));
        textContainer.add(textLbl);
        textContainer.add(saveString);
        mainContainer.add(textContainer);

        //ActionListener for saving the images to disk
        JButton saveButton = new JButton("Save the charts in folder " + System.getProperty("user.dir"));
        saveButton.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String saveName = saveString.getText();
                saveString.setText("");
                try {
                    //new SwingWrapper(drugSpreadChart).displayChart();
                    BitmapEncoder.saveBitmap(spreadChart, "./" + saveName + "CancerSpreadChart", BitmapFormat.PNG);
                    BitmapEncoder.saveBitmap(percentageChart, "./" + saveName + "CancerPercentageChart", BitmapFormat.PNG);
                    if (!uniform) {
                        BitmapEncoder.saveBitmap(drugSpreadChart, "./" + saveName + "DrugSpreadChart", BitmapFormat.PNG);
                        BitmapEncoder.saveBitmap(drugTotalChart, "./" + saveName + "DrugTotalChart", BitmapFormat.PNG);
                    }
                    Container c = getContentPane();
                    BufferedImage im = new BufferedImage(c.getWidth(), c.getHeight(), BufferedImage.TYPE_INT_ARGB);
                    c.paint(im.getGraphics());
                    ImageIO.write(im, "PNG", new File("./" + saveName + "Full.png"));
                } catch (Exception ioe) {
                    System.out.println("Error saving files: ");
                    System.out.println(ioe);
                }

            }
        });

        saveButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainContainer.add(saveButton);

        this.add(mainContainer);
        this.pack();
        this.setVisible(true);

    }
}
