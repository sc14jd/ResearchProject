import java.awt.*;

/*
 * Created on 15th February 2018
 * By Jordan Day
 * Class to run the entire Project from
 */
public class Project {

    // Initialise gui and complete report with gui results
    public static void main(String[] args) {
        //Allows the GUI to be invoked at a later time, calls a
//        Runnable() object, which is the current GUI
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
//Creates a new GUI using this Cellular Automata class
//                and ensures it is visible on screen
                CellularAutomata cellUI = new CellularAutomata();
                cellUI.InitialiseWindow();
                cellUI.setVisible(true);

            }
        });
    }
}
